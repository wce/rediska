// Все значения времени для стандартизации указаны в мс
module.exports = {
    redis: {
        host: '127.0.0.1',
        port: 6379
    },
    generator: {
        key: 'rediska:generator',
        expire: 5e3,
        check: 1e3,
        message: {
            key: 'rediska:messages',
            timer: 500
        }
    },
    handler: {
        delay: 10,
        failed: 0.05,
        error: {
            key: 'rediska:errors',
            chance: 0.05
        }
    }
};
