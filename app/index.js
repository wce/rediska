const App = require('App');

(async () => {
    try {
    	await App.init();
    } catch (e) {
        console.error(e.stack);
        process.exit(0);
    }
})();
